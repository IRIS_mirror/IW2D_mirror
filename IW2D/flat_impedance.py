from .interface import FlatIW2DInput
from .constants import fMP, twopiMP, Z0, twoMP, C, oneMP
from .utils import make_layer_arrays, Plane
from typing import Any,  Dict, Tuple
import numpy as np
import pandas as pd
from numpy.typing import ArrayLike
import itertools

from cppyy.gbl import amp, gsl_integration_workspace_alloc, memorycontainer, alphamn, gsl_integration_workspace_free, coefmn


def flat_nonlinear_plane_exponent_combinations(maximum_order: int, top_bottom_symmetry: bool):
    """Generator over all valid, non-zero combination of planes and coordinate powers for flat chamber impedance computations,
    for the given maximum order and symmetry.

    :param maximum_order: The maximum order of terms to generate. The sum of all four yielded exponents will be less or equal than this
    :type maximum_order: int
    :param top_bottom_symmetry: Flag set to True if the top and bottom layers are symmetric, and False if not
    :type top_bottom_symmetry: bool
    :yield: A tuple of 1 Plane and 5 ints: The plane (l,x,y), the source particle x and y exponents,
    the test particle x and y exponents, and the sum of all four exponents (for convenience)
    :rtype: Tuple[Plane, int, int, int, int, int]
    """
    # Loop over all planes (l,x,y)
    for plane in Plane:
        # Loop over powers of the coordinates
        for exp_x_source, exp_y_source, exp_x_test, exp_y_test in itertools.product(range(maximum_order+1), repeat=4):
            sum_exponents = exp_x_source + exp_y_source + exp_x_test + exp_y_test

            # Filter valid combinations
            if (
                (sum_exponents <= maximum_order) and
                ((exp_x_source + exp_x_test + (plane == Plane.X))%2 == 0) and
                ((( (sum_exponents + (plane != Plane.L))%2 )*top_bottom_symmetry) == 0)
            ):
                yield plane, exp_x_source, exp_y_source, exp_x_test, exp_y_test, sum_exponents
                

def flat_metadata(input_obj: FlatIW2DInput) -> Dict[str, Dict[str, Any]]:
    """Generate a dictionary of metadata for the impedances calculated from input_obj.

    :param input_obj: An input object.
    :type input_obj: FlatIW2DInput
    :return: A dict where the keys are the names of the impedance components calculated, and the values
    are another dict which contains the units, plane and exponents of the corresponding impedance component
    :rtype: Dict[str, Dict[str, Any]]
    """

    impedance_metadata: Dict[str, Dict[str, Any]] = {}

    if input_obj.maximum_order == 1:
        # Special case for order 1 for backwards compatibility.
        impedance_metadata["Zlong" ] = {"Units":"Ohm",   "Plane":Plane.L.value, "Exponents":(0,0,0,0)}
        impedance_metadata["Zycst" ] = {"Units":"Ohm",   "Plane":Plane.Y.value, "Exponents":(0,0,0,0)}
        impedance_metadata["Zxdip" ] = {"Units":"Ohm/m", "Plane":Plane.X.value, "Exponents":(1,0,0,0)}
        impedance_metadata["Zydip" ] = {"Units":"Ohm/m", "Plane":Plane.Y.value, "Exponents":(0,1,0,0)}
        impedance_metadata["Zxquad"] = {"Units":"Ohm/m", "Plane":Plane.X.value, "Exponents":(0,0,1,0)}
        impedance_metadata["Zyquad"] = {"Units":"Ohm/m", "Plane":Plane.Y.value, "Exponents":(0,0,0,1)}

    else:

        # Loop over valid plane and exponent combinations
        for plane, exp_x_source, exp_y_source, exp_x_test, exp_y_test, sum_exponents in flat_nonlinear_plane_exponent_combinations(input_obj.maximum_order, input_obj.top_bottom_symmetry):
            
            # Set units and file names
            if sum_exponents == 0:
                # Zeroth order terms
                unit = "Ohm"
                if plane==Plane.L:
                    name = f"Zlong"
                else:
                    name = f"Zycst" # necessarily, plane=="y" (it cannot be hor. because of the condition (a+c+(plane=="x"))%2 == 0)
            
            elif sum_exponents == 1:
                # First order terms
                unit = "Ohm/m"
                if ( exp_x_source==1 and plane==Plane.X ):
                    name = f"Zxdip"
                elif ( exp_y_source==1 and plane==Plane.Y ):
                    name = f"Zydip"
                elif ( exp_x_test==1 and plane==Plane.X ):
                    name = f"Zxquad"
                elif ( exp_y_test==1 and plane==Plane.Y ):
                    name = f"Zyquad"
                else:
                    unit = f"Ohm/m^{sum_exponents}"
                    name = f"Z{plane.value}{exp_x_source}{exp_y_source}{exp_x_test}{exp_y_test}"
            
            else:
                # Higher order terms
                unit = f"Ohm/m^{sum_exponents}"
                name = f"Z{plane.value}{exp_x_source}{exp_y_source}{exp_x_test}{exp_y_test}"
            
            # Fill in metadata dictionary
            impedance_metadata[name] = {"Units":unit, "Plane":plane.value, "Exponents":(exp_x_source, exp_y_source, exp_x_test, exp_y_test)}
    
    return impedance_metadata


def _iw2d_flat_impedance_single_frequency(
    input_obj: FlatIW2DInput,
    frequency: float,
    n_impedance_terms: int,
    integration_workspace_limit: int = 1000,
    maxmem: int = 50000
) -> np.ndarray:
    """### Description
    Calculate the impedance at the given frequency in a flat chamber setup defined in input_obj.

    ### Parameters
    input_obj : FlatIW2DInput
        An input object containing all relevant information about the setup
    frequency : float
        The frequency in Hz to evaluate the impedance at
    n_impedance_terms : number of terms that should be calculated. Note: This is predetermined by input_obj,
    but passing it explicitly as an argument allows more efficient allocation of arrays.

    ### Returns
    np.ndarray[complex]
        An array of the impedances for the given frequency. 
        The impedance terms are returned in the same order as the keys of the corresponding flat_metadata dict.
    """
    
    N = len(input_obj.top_layers)
    M = len(input_obj.bottom_layers) if input_obj.bottom_layers is not None else N
    gammaMP = fMP(input_obj.relativistic_gamma) # ampf
    beta = amp.sqrt(oneMP-oneMP/amp.sqr(gammaMP)) # ampf
    omega = twopiMP*fMP(frequency) # ampf
    k = omega/(beta*C) # ampf
    kovergamma = k/gammaMP # ampf

    w = gsl_integration_workspace_alloc(integration_workspace_limit)

    memory = memorycontainer(maxmem)


    # Calculate layer parameters for the top layers
    eps1, mu1, b = make_layer_arrays(input_obj.top_layers, input_obj.top_half_gap, frequency)

    # Calculate layer parameters for the bottom layers. Note that b0 must be negative
    if input_obj.top_bottom_symmetry:
        eps1m, mu1m, bm = make_layer_arrays(input_obj.top_layers, input_obj.top_half_gap, frequency, b_negative=True)
    else:
        eps1m, mu1m, bm = make_layer_arrays(input_obj.bottom_layers, input_obj.bottom_half_gap, frequency, b_negative=True) # type: ignore

    if input_obj.maximum_order == 1:

        # Compute alphamn needed for first order impedances (in C++)
        alpha00 = alphamn(input_obj.top_bottom_symmetry,M,N,b,bm,beta,eps1,eps1m,mu1,mu1m,omega,k,kovergamma,0,0,integration_workspace_limit,w,memory) # complex
        alpha01 = alphamn(input_obj.top_bottom_symmetry,M,N,b,bm,beta,eps1,eps1m,mu1,mu1m,omega,k,kovergamma,0,1,integration_workspace_limit,w,memory) # complex
        alpha02 = alphamn(input_obj.top_bottom_symmetry,M,N,b,bm,beta,eps1,eps1m,mu1,mu1m,omega,k,kovergamma,0,2,integration_workspace_limit,w,memory) # complex
        alpha11 = alphamn(input_obj.top_bottom_symmetry,M,N,b,bm,beta,eps1,eps1m,mu1,mu1m,omega,k,kovergamma,1,1,integration_workspace_limit,w,memory) # complex

        # Compute all impedances from the alphas
        prefactor = 1j*input_obj.length*(k*Z0/(beta*amp.sqr(gammaMP)*twopiMP)).toDouble() # complex
        Zlong = prefactor*alpha00 # complex
        Zycst = prefactor*alpha01/(gammaMP.toDouble()) # complex
        prefactor = prefactor*(k/(twoMP*amp.sqr(gammaMP))).toDouble()  # complex
        Zxdip = prefactor*(alpha02-alpha00) # complex
        Zydip = 2*prefactor*alpha11 # complex
        Zxquad = -Zxdip # complex
        Zyquad =prefactor*(alpha02+alpha00) # complex

        # Free memory
        gsl_integration_workspace_free(w)

        return np.array([Zlong, Zycst, Zxdip, Zydip, Zxquad, Zyquad], dtype=complex)
    
    else:
        # order != 1

        # Set up output array
        Z = np.zeros(n_impedance_terms, dtype=complex)

        # Set up dict for caching alpha values, as they for a given freq only depend on m and n
        alphas: Dict[Tuple[int,int], complex] = {}

        # Compute gamma (energy) dependent coefficient
        coef_gamma = 4j*input_obj.length*(Z0/(beta*amp.sqr(gammaMP)*twopiMP)).toDouble() # complex

        # Loop over valid planes and coordinate exponents
        for i, (plane, exp_x_source, exp_y_source, exp_x_test, exp_y_test, sum_exponents) \
            in enumerate(flat_nonlinear_plane_exponent_combinations(input_obj.maximum_order, input_obj.top_bottom_symmetry)):
            
            # Compute plane and exponent dependent coefficient
            if plane == Plane.L:
                coef_plane = k.toDouble()
            elif plane == Plane.X:
                coef_plane = exp_x_test + 1.0
            else:
                coef_plane = exp_y_test + 1.0

            # Loop over r and q
            for r in range(exp_y_source//2 + 1):
                for q in range((exp_x_source + exp_x_test + (plane == Plane.X))//2 + (exp_y_test + (plane==Plane.Y))//2 + 1):

                    # Compute m, n, and a coefficient dependent on those
                    m = exp_y_source - 2*r
                    n = exp_x_source + exp_x_test + exp_y_test + (plane != Plane.L) - 2*q
                    coef_mn = coefmn(exp_x_source, exp_y_source, exp_x_test+(plane==Plane.X), exp_y_test+(plane==Plane.Y), m, n, r, q)
    
                    # Calculate alpha coefficienct (in c++)
                    if (m,n) not in alphas:
                        alphas[(m,n)] = alphamn(input_obj.top_bottom_symmetry,M,N,b,bm,beta,eps1,eps1m,mu1,mu1m,omega,k,kovergamma,m,n,integration_workspace_limit,w,memory)
                    
                    # Calculate impedance contribution
                    Z[i] = Z[i] + alphas[(m,n)]*coef_gamma*coef_plane*coef_mn*(kovergamma.toDouble()/2.)**(sum_exponents + (plane != Plane.L))

        # free memory used for integration
        gsl_integration_workspace_free(w)

        return Z


def iw2d_flat_impedance(
    input_obj: FlatIW2DInput,
    frequencies: ArrayLike,
    integration_workspace_limit: int = 1000,
    maxmem: int = 50000
) -> Tuple[pd.DataFrame, Dict[str, Dict[str, Any]]]:
    """### Description
    Calculate impedances for the frequencies given as input for a flat chamber and return in a pandas.DataFrame.

    Also returns a metadata dictionary with units, planes and exponents.

    ### Parameters
    input_obj : FlatIW2DInput
        A FlatIW2DInput object
    frequencies : ArrayLike
        A single frequency or an iterable of frequencies in Hz

    ### Returns
    Tuple[pandas.DataFrame, Dict[str, Dict[str, Any]]]
        First element is a DataFrame with frequencies (float) as index and impedances (complex) as values
        Second element is a dictionary with the DataFrame column names as keys,
        and as values dictionaries containing the metadata for that column (units, plane, exponents)
    """

    # make index series of frequencies. Works for both iterable and non-iterable frequencies
    frequency_series = pd.Series(data=frequencies, dtype=float, name="Frequency [Hz]")
    
    # create metadata dictionary
    impedance_metadata: Dict[str, Dict[str, Any]] = flat_metadata(input_obj)
    
    n_impedance_terms = len(impedance_metadata)

    # initialize array of impedances
    impedances = np.zeros((len(frequency_series), n_impedance_terms), dtype=complex)

    # loop over frequencies and calculate
    for i, frequency in enumerate(frequency_series):
        impedances[i,:] = _iw2d_flat_impedance_single_frequency(input_obj, frequency, n_impedance_terms, integration_workspace_limit, maxmem)

    # create dataframe
    impedance_dataframe = pd.DataFrame(
        data=impedances,
        index=frequency_series,
        columns=impedance_metadata.keys()
    )

    return impedance_dataframe, impedance_metadata
